/*
 * gateway_comms.c
 *
 * Created: 30/09/2019 16:05:11
 *  Author: James Messenger
 */ 
#include <atmel_start.h>
#include <string.h>
#include <hal_io.h>
#include <stdio.h>
#include <ctype.h>
#include <hal_usart_sync.h>
#include <utils.h>
#include "gateway_comms.h"

void Gateway_Read(int8_t* str)
{
	struct io_descriptor *io;
	int8_t a = 0;
	usart_sync_get_io_descriptor(&GATEWAY_RS485, &io);
	usart_sync_enable(&GATEWAY_RS485);

	while(a < 10) //max length of 10
	{
		io_read(io, &str[a], 1);
		if (str[a] == '\n')
		{
			str[a] = 0;
			break;
		}
		a++;
	}

}

void Gateway_GetCharacter(int8_t* str)
{
	struct io_descriptor *io;
	int8_t a = 0;
	usart_sync_get_io_descriptor(&GATEWAY_RS485, &io);
	usart_sync_enable(&GATEWAY_RS485);

	io_read(io, &str[a], 1);

	
	
}

//Function for printing a string over the ACM USART. The string can have a number inserted at a location given by "%i" (second argument to the function)
void Gateway_Printf(int8_t *str, uint32_t number)
{
	struct io_descriptor *io;
	int8_t numberPointer = 0;
	int8_t stringLength = 0;
	int8_t flag = 0;
	int8_t count;
	int8_t numberString[10];
	
	usart_sync_get_io_descriptor(&GATEWAY_RS485, &io);
	usart_sync_enable(&GATEWAY_RS485);
	
	gpio_set_pin_level(GATEWAY_RS485_nRE,true);
	
	while(*(str + stringLength) != 0)
	{
		if (*(str + stringLength) == '%')
		{
			if (*(str + stringLength + 1) == 'i')
			{
				numberPointer = stringLength;
				flag = 1;
			}
		}
		if (stringLength == 255)
		break;
		stringLength++;
	}
	
	//print the string
	
	if (flag) //Number present
	{
		//print up to the number
		for(count = 0;count<numberPointer;count++)
		{
			io_write(io, (uint8_t*)str + count, 1);
		}
		
		sprintf(numberString, "%d", number);
		//print the number
		for(count = 0;count<strlen(numberString);count++)
		{
			io_write(io, (uint8_t*)numberString + count, 1);
		}
		
		//print the rest
		for(count = numberPointer+2;count<stringLength;count++)
		{
			io_write(io, (uint8_t*)str + count, 1);
		}
	}
	else
	{
		for(count = 0;count<stringLength;count++)
		{
			io_write(io, (uint8_t*)str + count, 1);
		}
		
	}
	usart_sync_disable(&GATEWAY_RS485);
	delay_ms(10);
	gpio_set_pin_level(GATEWAY_RS485_nRE,false);
}

int8_t check_numeric(int8_t *str)
{
	if(*str == 0) return 0;
	
	while(*str)
	{
		if(!isdigit(*str++)) return 0;
	}
	return 1;
}

void Actuator_RS485_TestTx(void)
{
	struct io_descriptor *io;
	usart_sync_get_io_descriptor(&ACTUATOR_RS485, &io);
	usart_sync_enable(&ACTUATOR_RS485);
	gpio_set_pin_level(ENCODER_RS485_nRE, true);
	io_write(io, (uint8_t *)"\r\n\033[H\033[2JActuator RS485 test. Respond with 123\r\n", 48);
	gpio_set_pin_level(ENCODER_RS485_nRE, false);
}


uint8_t Actuator_RS485_TestRx(int8_t* str, int8_t len)
{
	struct io_descriptor *io;
	usart_sync_get_io_descriptor(&ACTUATOR_RS485, &io);
	usart_sync_disable(&ACTUATOR_RS485); //Disable then re-enable to clear any characters that may be in the buffer already
	usart_sync_enable(&ACTUATOR_RS485);
	return(usart_sync_read_with_timeout(&ACTUATOR_RS485, str, len));
	//io_read(io, str, len);

}

//Copied from hal_usart_sync.c, with edits for timeout
int32_t usart_sync_read_with_timeout(struct io_descriptor *const io_descr, uint8_t *const buf, const uint16_t length)
{
	uint32_t offset = 0;
	uint8_t count = 0;
	uint8_t count_max = 250;
	struct usart_sync_descriptor *descr  = CONTAINER_OF(io_descr, struct usart_sync_descriptor, io);

	ASSERT(io_descr && buf && length);
	do {
		while (!_usart_sync_is_byte_received(&descr->device))
		{
			delay_ms(20);
			count++;
			if (count >= count_max) //Timeout
				{
					return(0);
				}
		}
		buf[offset] = _usart_sync_read_byte(&descr->device);
		count = 0;
		
	} while (++offset < length);

	return (int32_t)offset;
}