/*
 * gateway_comms.h
 *
 * Created: 30/09/2019 16:05:53
 *  Author: James Messenger
 */ 


#ifndef GATEWAYCOMMS_H_
#define GATEWAYCOMMS_H_


void Gateway_Read(int8_t* str);
void Gateway_GetCharacter(int8_t* str);
void Gateway_Printf(int8_t *str, uint32_t number);
int8_t check_numeric(int8_t *str);
void Actuator_RS485_TestTx(void);
uint8_t Actuator_RS485_TestRx(int8_t* str, int8_t len);
int32_t usart_sync_read_with_timeout(struct io_descriptor *const io_descr, uint8_t *const buf, const uint16_t length);


#endif 