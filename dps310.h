/*/*
 * \file dps310.h
 *
 * \brief Types and constants definitions for the DPS310 Ambient Pressure component
 *
 * Definitions of registers and configuration values are created based on the product datasheet
 * https://www.infineon.com/dgdl/Infineon-DPS310-DS-v01_00-EN.pdf?fileId=5546d462576f34750157750826c42242
 * (V 1.1, 11/07/2019)
 *
 * The default configuration for the sensor is set to sample pressure and temperature as a
 * background process, obtaining one sample per second. The oversampling is configured as 2 bits
 * for the pressure measurements and 0 bits for the temperature (single measurement). This leads
 * to a typical current consumption of 4.5 uAh. If a lower power mode is required, oversampling can
 * be turned down completely and the current consumption would go down to 3 uAh. 
 *
 * \date 30/09/2016
 * \author Alberto
 */ 


#ifndef DPS310_H_
#define DPS310_H_

#include <stdbool.h>
#include <stdint.h>

/* ----------------------------------- DPS310 Data Structures ----------------------------------- */
/*! \brief DPS310 Coefficients placeholder to store the coefficients */
typedef struct __dpsCoeff
{
	int16_t temperature_c0;	/*!< \brief Coefficient C0 for Temperature */
	int16_t temperature_c1;	/*!< \brief Coefficient C1 for Temperature */
	int32_t pressure_c00;		/*!< \brief Coefficient C00 for Pressure */
	int32_t pressure_c10;		/*!< \brief Coefficient C10 for Pressure */
	int16_t pressure_c01;		/*!< \brief Coefficient C01 for Pressure */
	int16_t pressure_c11;		/*!< \brief Coefficient C11 for Pressure */
	int16_t pressure_c20;		/*!< \brief Coefficient C20 for Pressure */
	int16_t pressure_c21;		/*!< \brief Coefficient C21 for Pressure */
	int16_t pressure_c30;		/*!< \brief Coefficient C30 for Pressure */
} DPS310_Coeffs_t;

//#include "sensors_common.h"

int16_t dps310_read_pressure_coeff(uint8_t *const buffer);
int32_t dps310_calc_pressure(int32_t raw_p, int32_t raw_t);
int32_t dps310_calc_temperature(int32_t raw_t);
void dps310_read_pressure_long_coeffs(DPS310_Coeffs_t *const coeff, uint8_t *const buffer);
void dps310_read_temperature_coeffs(DPS310_Coeffs_t *const coeff, uint8_t *const buffer);
void dps310_get_coefficients();


/* -------------------------- DPS310 Registers and offset definitions --------------------------- */
#define DPS310_BASE_ADDRESS				((uint8_t) 0x00)	/* starting address for the registers */
#define DPS310_PRESSURE_OFFSET		((uint8_t) 0x00)	/* offset for the pressure reading */
#define DPS310_TEMPERATURE_OFFSET	((uint8_t) 0x03)	/* offset for the temperature reading */
#define DPS310_CONFIG_OFFSET			((uint8_t) 0x06)	/* offset for the configuration settings */
#define DPS310_MEAS_CFG_OFFSET		((uint8_t) 0x08)	/* offset for the configuration settings */
#define DPS310_COEFF_OFFSET				((uint8_t) 0x10)	/* offset for the sensors coefficients */
#define DPS310_MEAS_LENGTH				((uint16_t)   3)	/* length (in Bytes) of a measurement */
#define DPS310_ADDRESS_LENGTH			((uint16_t)   1)	/* address length in Bytes */
#define DPS310_CONFIG_LENGTH			((uint16_t)   4)	/* Length of the configuration registers */
#define DPS310_COEFF_LENGTH				((uint16_t)  26)	/* Length of the coefficients registers */
#define DPS310_P_OFFSET						((uint8_t)    0)	/* Offset of the Pressure Measure in resp. */
#define DPS310_T_OFFSET						((uint8_t)    3)	/* Offset of the Temp. Measure in resp. */
#define DPS310_C0_OFFSET					((uint8_t)    0)	/* Offset of C0 in resp. */
#define DPS310_C00_OFFSET					((uint8_t)    3)	/* Offset of C00 in resp. */
#define DPS310_C01_OFFSET					((uint8_t)    8)	/* Offset of C01 in resp. */
#define DPS310_C11_OFFSET					((uint8_t)   10)	/* Offset of C11 in resp. */
#define DPS310_C20_OFFSET					((uint8_t)   12)	/* Offset of C20 in resp. */
#define DPS310_C21_OFFSET					((uint8_t)   14)	/* Offset of C21 in resp. */
#define DPS310_C30_OFFSET					((uint8_t)   16)	/* Offset of C30 in resp. */
#define DPS310_RX_DELAY_MS				((uint16_t)   0)	/* Delay between TX and RX */
#define DPS310_SIGNATURE_OFFSET		((uint8_t) 0x0e)	/* Offset for signature (to enable write) */
#define DPS310_HIGH_T_GAIN_OFFSET	((uint8_t) 0x62)	/* Offset for high-gain T */
#define DPS310_SIGNATURE_LENGTH		((uint16_t)   2)	/* Length for signature (to enable write) */
#define DPS310_HIGH_T_GAIN_LENGTH	((uint16_t)   1)	/* Length for high-gain T */
#define DPS310_SET_STANDBY_LENGTH	((uint16_t)   1)	/* Length for high-gain T */
#define DPS310_CHECK_COEFF_LENGTH	((uint16_t)   1)	/* Length for the coefficient check */


/* -------------------- Bitfield definitions for the configuration registers -------------------- */
/* Definitions for PRS_CFG and TMP_CFG registers */
#define DPS310_CFG_PRC_gp					((uint8_t)    0)
#define DPS310_CFG_PRC_gm					((uint8_t) 0x0F)
#define DPS310_CFG_RATE_gp				((uint8_t)    4)
#define DPS310_CFG_RATE_gm				((uint8_t) 0x70)
#define DPS310_CFG_TMP_EXT_bp			((uint8_t)    7)
#define DPS310_CFG_TMP_EXT_bm			((uint8_t) 0x80)
#define DPS310_MEAS_CTRL_gp				((uint8_t)    0)
#define DPS310_MEAS_CTRL_gm				((uint8_t) 0x07)

/* Oversampling */
#define DPS310_CFG_PRC_SINGLE			((uint8_t)(0 << DPS310_CFG_PRC_gp))
#define DPS310_CFG_PRC_2TIMES			((uint8_t)(1 << DPS310_CFG_PRC_gp))
#define DPS310_CFG_PRC_4TIMES			((uint8_t)(2 << DPS310_CFG_PRC_gp))
#define DPS310_CFG_PRC_8TIMES			((uint8_t)(3 << DPS310_CFG_PRC_gp))
#define DPS310_CFG_PRC_16TIMES		((uint8_t)(4 << DPS310_CFG_PRC_gp))
#define DPS310_CFG_PRC_32TIMES		((uint8_t)(5 << DPS310_CFG_PRC_gp))
#define DPS310_CFG_PRC_64TIMES		((uint8_t)(6 << DPS310_CFG_PRC_gp))
#define DPS310_CFG_PRC_128TIMES		((uint8_t)(7 << DPS310_CFG_PRC_gp))

/* Samples per second */
#define DPS310_CFG_RATE_1SPS			((uint8_t)(0 << DPS310_CFG_RATE_gp))
#define DPS310_CFG_RATE_2SPS			((uint8_t)(1 << DPS310_CFG_RATE_gp))
#define DPS310_CFG_RATE_4SPS			((uint8_t)(2 << DPS310_CFG_RATE_gp))
#define DPS310_CFG_RATE_8SPS			((uint8_t)(3 << DPS310_CFG_RATE_gp))
#define DPS310_CFG_RATE_16SPS			((uint8_t)(4 << DPS310_CFG_RATE_gp))
#define DPS310_CFG_RATE_32SPS			((uint8_t)(5 << DPS310_CFG_RATE_gp))
#define DPS310_CFG_RATE_64SPS			((uint8_t)(6 << DPS310_CFG_RATE_gp))
#define DPS310_CFG_RATE_128SPS		((uint8_t)(7 << DPS310_CFG_RATE_gp))

/* Internal or external sensor used for temperature */
#define DPS310_CFG_TMP_INTERNAL		((uint8_t)(0 << DPS310_CFG_TMP_EXT_bp))
#define DPS310_CFG_TMP_EXTERNAL		((uint8_t)(1 << DPS310_CFG_TMP_EXT_bp))

/* Measurement Modes and Types (CMD: Command, BGD: Background) */
#define DPS310_MEAS_CTRL_STANDBY	((uint8_t) (0 << DPS310_MEAS_CTRL_gp))
#define DPS310_MEAS_CTRL_CMD_PRS	((uint8_t) (1 << DPS310_MEAS_CTRL_gp))
#define DPS310_MEAS_CTRL_CMD_TMP	((uint8_t) (2 << DPS310_MEAS_CTRL_gp))
#define DPS310_MEAS_CTRL_BGD_PRS	((uint8_t) (5 << DPS310_MEAS_CTRL_gp))
#define DPS310_MEAS_CTRL_BGD_TMP	((uint8_t) (6 << DPS310_MEAS_CTRL_gp))
#define DPS310_MEAS_CTRL_BGD_BOTH	((uint8_t) (7 << DPS310_MEAS_CTRL_gp))


/* ------------------------- DPS310 Initialisation Registers and Masks -------------------------- */
#define DPS310_SIGNATURE_ENABLE0	((uint8_t) 0xa5)	/* First configuration byte to enable hidden write */
#define DPS310_SIGNATURE_ENABLE1	((uint8_t) 0x96)	/* Second configuration byte to enable hidden write */
#define DPS310_SIGNATURE_DISABLE	((uint8_t) 0x00)	/* Configuration byte to disable hidden write */
#define DPS310_HIGH_T_GAIN_ENABLE ((uint8_t) 0x02)	/* Configuration byte to enable high gain for temperature */
#define DPS310_COEFF_READY_MASK		((uint8_t) 0x80)	/* Mask to check if the coefficients are ready */
#define DPS310_SENSOR_READY_MASK	((uint8_t) 0x40)	/* Mask to check if the sensor is ready */


/* ----------------------- DPS310 Bit handling for C0 and C1 coefficients ----------------------- */
#define DPS310_C0_MSB_MASK	((uint16_t) 0x0ff0)	/* Mask for parsing the C0 coefficient MSB */
#define DPS310_C0_MSB_POS		((uint8_t) 4)				/* Position (bit) of the C0 MSB */
#define DPS310_C0_LSB_MASK	((uint16_t) 0x000f)	/* Mask for parsing the C0 coefficient LSB */
#define DPS310_C0_LSB_SHFT	((uint8_t) 4)				/* Shift (bits) for the C0 LSB */
#define DPS310_C1_MSB_MASK	((uint16_t) 0x0f00)	/* Mask for parsing the C1 coefficient MSB */
#define DPS310_C1_MSB_POS		((uint8_t) 8)				/* Position (bit) of the C1 MSB */
#define DPS310_C1_LSB_MASK	((uint16_t) 0x00ff)	/* Mask for parsing the C1 coefficient LSB */
#define DPS310_C1_LSB_SHFT	((uint8_t) 0)				/* Shift (bits) for the C1 LSB */
#define DPS310_SIGN_MASK		((uint16_t) 0x0800)	/* Sign position for the 12-bit coefficients */
#define DPS310_SIGN_EXT			((uint16_t) 0xf000)	/* Sign extension for the 12-bit coefficients */


/* ---------------------- DPS310 Bit handling for C00 and C10 coefficients ---------------------- */
#define DPS310_C00_MSB_MASK		((uint32_t) 0x000ff000)	/* Mask for parsing the C00 coeff. MSB */
#define DPS310_C00_MSB_POS		((uint8_t) 12)					/* Position (bit) of the C00 MSB */
#define DPS310_C00_HSB_MASK		((uint32_t) 0x00000ff0)	/* Mask for parsing the C00 coeff. LSB */
#define DPS310_C00_HSB_POS		((uint8_t) 4)						/* Position (bit) of the C00 LSB */
#define DPS310_C00_LSB_MASK		((uint32_t) 0x0000000f)	/* Mask for parsing the C00 coeff. XLSB */
#define DPS310_C00_LSB_SHFT		((uint8_t) 4)						/* Shift (bits) for the C00 XLSB */
#define DPS310_C10_MSB_MASK		((uint32_t) 0x000f0000)	/* Mask for parsing the C10 coeff. MSB */
#define DPS310_C10_MSB_POS		((uint8_t) 16)					/* Position (bit) of the C10 MSB */
#define DPS310_C10_HSB_MASK		((uint32_t) 0x0000ff00)	/* Mask for parsing the C10 coeff. LSB */
#define DPS310_C10_HSB_POS		((uint8_t) 8)						/* Position (bit) of the C10 LSB */
#define DPS310_C10_LSB_MASK		((uint32_t) 0x000000ff)	/* Mask for parsing the C10 coeff. XLSB */
#define DPS310_C10_LSB_SHFT		((uint8_t) 0)						/* Shift (bits) for the C10 XLSB */
#define DPS310_LONG_SIGN_MASK	((uint32_t) 0x00080000)	/* Sign position for the 24-bit coeff. */
#define DPS310_LONG_SIGN_EXT	((uint32_t) 0xfff00000)	/* Sign extension for the 24-bit coeff. */


/* ---------------------- DPS310 Bit handling for Cxx 16 bits coefficients ---------------------- */
#define DPS310_PCOEFF_MSB_MASK ((uint16_t) 0xff00)	/* Mask for parsing the Cxx coefficient MSB */
#define DPS310_PCOEFF_LSB_MASK ((uint16_t) 0x00ff)	/* Mask for parsing the Cxx coefficient LSB */
#define DPS310_PCOEFF_MSB_POS	 ((uint8_t) 8)				/* Position (bit) of the Cxx MSB */
#define DPS310_PCOEFF_LSB_POS	 ((uint8_t) 0)				/* Position (bit) of the Cxx LSB */


/* ------------------------ DPS310 Bit handling for 24 bits measurements ------------------------ */
#define DPS310_MEAS_MSB_MASK	((uint32_t) 0x00ff0000)	/* Mask for parsing the measurement MSB */
#define DPS310_MEAS_LSB_MASK	((uint32_t) 0x0000ff00)	/* Mask for parsing the measurement LSB */
#define DPS310_MEAS_XLSB_MASK ((uint32_t) 0x000000ff)	/* Mask for parsing the measurement XLSB */
#define DPS310_MEAS_MSB_POS		((uint8_t) 16)					/* Position (bit) of the measurement MSB */
#define DPS310_MEAS_LSB_POS		((uint8_t)  8)					/* Position (bit) of the measurement LSB */
#define DPS310_MEAS_XLSB_POS	((uint8_t)  0)					/* Position (bit) of the measurement XLSB */
#define DPS310_READ_SIGN_MASK	((uint32_t) 0x00800000)	/* Sign position for the 24-bit coeff. */
#define DPS310_READ_SIGN_EXT	((uint32_t) 0xff000000)	/* Sign extension for the 24-bit coeff. */

/* --------------------------------- DPS310 Scaling Factors ------------------------------------- */
#define DPS310_SCALING_0BIT_OVS ((uint32_t)  524288)
#define DPS310_SCALING_1BIT_OVS ((uint32_t) 1572864)
#define DPS310_SCALING_2BIT_OVS ((uint32_t) 3670016)
#define DPS310_SCALING_3BIT_OVS ((uint32_t) 7864320)
#define DPS310_SCALING_4BIT_OVS ((uint32_t)  252952)
#define DPS310_SCALING_5BIT_OVS ((uint32_t)  516096)
#define DPS310_SCALING_6BIT_OVS ((uint32_t) 1040384)
#define DPS310_SCALING_7BIT_OVS ((uint32_t) 2088960)

/* ------------------- Temperature constants used to calculate the temperature ------------------ */
#define DPS310_TMP_RAW_MULT						((uint8_t)			2)
#define DPS310_TMP_NUM_MULT						((uint16_t)		500)
#define DPS310_CELSIUS_TO_MILLIKELVIN	((int32_t) 273150)	/* millikelvin degrees for 0.0 deg C */


/* ------------------- DPS310 Default values for the configuration registers -------------------- */
/* The following definitions configure the measurement behaviour of the sensor */
/* Pressure Acquisition Configuration */
#define DPS310_PRS_SAMPLE_RATE	DPS310_CFG_RATE_1SPS
#define DPS310_PRS_OVS_N_BITS		2			/* Number of oversampling bits to configure */

/* Temperature Acquisition Configuration */
#define DPS310_TMP_SOURCE				DPS310_CFG_TMP_EXTERNAL
#define DPS310_TMP_SAMPLE_RATE	DPS310_CFG_RATE_1SPS
#define DPS310_TMP_OVS_N_BITS		0			/* Number of oversampling bits to configure */

/* Acquisition Mode Configuration */
#define DPS310_ACQUISITION_MODE		DPS310_MEAS_CTRL_BGD_BOTH

/* End of the configuration definitions. Do not alter any of the following definitions, as they are
* generated based on the configuration set above */

/* Configuration-defined definitions to configure the sensor based on the selected option */
#if (DPS310_TMP_OVS_N_BITS == 0)
	#define DPS310_TEMPERATURE_SCALING	DPS310_SCALING_0BIT_OVS
	#define DPS310_TMP_OVS_CFG					DPS310_CFG_PRC_SINGLE
	#define DPS310_TEMPERATURE_SHIFT		((uint8_t) 0x00)
#elif (DPS310_TMP_OVS_N_BITS == 1)
	#define DPS310_TEMPERATURE_SCALING	DPS310_SCALING_1BIT_OVS
	#define DPS310_TMP_OVS_CFG					DPS310_CFG_PRC_2TIMES
	#define DPS310_TEMPERATURE_SHIFT		((uint8_t) 0x00)
#elif (DPS310_TMP_OVS_N_BITS == 2)
	#define DPS310_TEMPERATURE_SCALING	DPS310_SCALING_2BIT_OVS
	#define DPS310_TMP_OVS_CFG					DPS310_CFG_PRC_4TIMES
	#define DPS310_TEMPERATURE_SHIFT		((uint8_t) 0x00)
#elif (DPS310_TMP_OVS_N_BITS == 3)
	#define DPS310_TEMPERATURE_SCALING	DPS310_SCALING_3BIT_OVS
	#define DPS310_TMP_OVS_CFG					DPS310_CFG_PRC_8TIMES
	#define DPS310_TEMPERATURE_SHIFT		((uint8_t) 0x00)
#elif (DPS310_TMP_OVS_N_BITS == 4)
	#define DPS310_TEMPERATURE_SCALING	DPS310_SCALING_4BIT_OVS
	#define DPS310_TMP_OVS_CFG					DPS310_CFG_PRC_16TIMES
	#define DPS310_TEMPERATURE_SHIFT		((uint8_t) 0x04)
#elif (DPS310_TMP_OVS_N_BITS == 5)
	#define DPS310_TEMPERATURE_SCALING	DPS310_SCALING_5BIT_OVS
	#define DPS310_TMP_OVS_CFG					DPS310_CFG_PRC_32TIMES
	#define DPS310_TEMPERATURE_SHIFT		((uint8_t) 0x04)
#elif (DPS310_TMP_OVS_N_BITS == 6)
	#define DPS310_TEMPERATURE_SCALING	DPS310_SCALING_6BIT_OVS
	#define DPS310_TMP_OVS_CFG					DPS310_CFG_PRC_64TIMES
	#define DPS310_TEMPERATURE_SHIFT		((uint8_t) 0x04)
#elif (DPS310_TMP_OVS_N_BITS == 7)
	#define DPS310_TEMPERATURE_SCALING	DPS310_SCALING_7BIT_OVS
	#define DPS310_TMP_OVS_CFG					DPS310_CFG_PRC_128TIMES
	#define DPS310_TEMPERATURE_SHIFT		((uint8_t) 0x04)
#else
	#error "DPS310 temperature oversampling configuration not supported"
#endif // DPS310_TMP_OVS_N_BITS


#if (DPS310_PRS_OVS_N_BITS == 0)
	#define DPS310_PRESSURE_SCALING	DPS310_SCALING_0BIT_OVS
	#define DPS310_PRS_OVS_CFG			DPS310_CFG_PRC_SINGLE
	#define DPS310_PRESSURE_SHIFT		((uint8_t) 0x00)
#elif (DPS310_PRS_OVS_N_BITS == 1)
	#define DPS310_PRESSURE_SCALING	DPS310_SCALING_1BIT_OVS
	#define DPS310_PRS_OVS_CFG			DPS310_CFG_PRC_2TIMES
	#define DPS310_PRESSURE_SHIFT		((uint8_t) 0x00)
#elif (DPS310_PRS_OVS_N_BITS == 2)
	#define DPS310_PRESSURE_SCALING	DPS310_SCALING_2BIT_OVS
	#define DPS310_PRS_OVS_CFG			DPS310_CFG_PRC_4TIMES
	#define DPS310_PRESSURE_SHIFT		((uint8_t) 0x00)
#elif (DPS310_PRS_OVS_N_BITS == 3)
	#define DPS310_PRESSURE_SCALING	DPS310_SCALING_3BIT_OVS
	#define DPS310_PRS_OVS_CFG			DPS310_CFG_PRC_8TIMES
	#define DPS310_PRESSURE_SHIFT		((uint8_t) 0x00)
#elif (DPS310_PRS_OVS_N_BITS == 4)
	#define DPS310_PRESSURE_SCALING	DPS310_SCALING_4BIT_OVS
	#define DPS310_PRS_OVS_CFG			DPS310_CFG_PRC_16TIMES
	#define DPS310_PRESSURE_SHIFT		((uint8_t) 0x04)
#elif (DPS310_PRS_OVS_N_BITS == 5)
	#define DPS310_PRESSURE_SCALING	DPS310_SCALING_5BIT_OVS
	#define DPS310_PRS_OVS_CFG			DPS310_CFG_PRC_32TIMES
	#define DPS310_PRESSURE_SHIFT		((uint8_t) 0x04)
#elif (DPS310_PRS_OVS_N_BITS == 6)
	#define DPS310_PRESSURE_SCALING	DPS310_SCALING_6BIT_OVS
	#define DPS310_PRS_OVS_CFG			DPS310_CFG_PRC_64TIMES
	#define DPS310_PRESSURE_SHIFT		((uint8_t) 0x04)
#elif (DPS310_PRS_OVS_N_BITS == 7)
	#define DPS310_PRESSURE_SCALING	DPS310_SCALING_7BIT_OVS
	#define DPS310_PRS_OVS_CFG			DPS310_CFG_PRC_128TIMES
	#define DPS310_PRESSURE_SHIFT		((uint8_t) 0x04)
#else
	#error "DPS310 pressure oversampling configuration not supported"
#endif // DPS310_PRS_OVS_N_BITS

/* Assembly of registers values to be written as part of the configuration write */
#define DPS310_CONFIG_PRS_CFG		(DPS310_PRS_SAMPLE_RATE | DPS310_PRS_OVS_CFG)
#define DPS310_CONFIG_TMP_CFG		(DPS310_TMP_SAMPLE_RATE | DPS310_TMP_OVS_CFG | DPS310_TMP_SOURCE)
#define DPS310_CONFIG_MEAS_CFG	DPS310_ACQUISITION_MODE
#define DPS310_CONFIG_CFG_REG		(DPS310_PRESSURE_SHIFT | DPS310_TEMPERATURE_SHIFT)


/* --------------------------- DPS310 Enumerated types for the device --------------------------- */
/*! \brief Commands for the DPS310 when using I2C communication */
typedef enum _dpsAct
{
	DPS310_WRITE_CONFIG,					/*!< Command for the DPS310: Write Configuration */
	DPS310_READ_PRESSURE,					/*!< Command for the DPS310: Read Pressure Measure */
	DPS310_READ_TEMPERATURE,			/*!< Command for the DPS310: Read Temperature Measure */
	DPS310_READ_P_AND_T,					/*!< Command for the DPS310: Read P and T Measures */
	DPS310_READ_COEFFICIENTS,			/*!< Command for the DPS310: Read Coefficients */
	DPS310_ENABLE_HIDDEN_WRITE,		/*!< Command for the DPS310: Read Coefficients */
	DPS310_DISABLE_HIDDEN_WRITE,	/*!< Command for the DPS310: Read Coefficients */
	DPS310_SET_STANDBY_MODE,			/*!< Command for the DPS310: Configure Standby mode */
	DPS310_CONFIGURE_HIGH_T_GAIN,	/*!< Command for the DPS310: Configure High Gain for T */
	DPS310_CHECK_COEFF_READY,			/*!< Command to check if the DPS coefficients are ready */
	DPS310_WAIT_SENSOR_READY,			/*!< Command to check if the DPS coefficients are ready */
	DPS310_SUPPORTED_ACTIONS_NO		/*!< Number of supported commands */
} DPS310_Action_t;




#endif /* DPS310_H_ */
