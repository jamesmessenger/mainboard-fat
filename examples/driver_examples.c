/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_examples.h"
#include "driver_init.h"
#include "utils.h"
#include "gateway_comms.h"

static uint8_t buf[16] = {0x0};
	
static volatile bool transaction_done;
static volatile bool transaction_error;

static void xfer_complete_cb_QUAD_SPI_0(struct _dma_resource *resource)
{
	transaction_done = true;
}
static void error_cb_QUAD_SPI_0(struct _dma_resource *resource)
{
	transaction_error = true;
}

/**
 * Example of using QUAD_SPI_0 to get N25Q256A status value,
 * and check bit 0 which indicate embedded operation is busy or not.
 */
void QUAD_SPI_0_example(void)
{
	static uint8_t qspi_buffer[3];
	const struct _qspi_command cmd = {
		.inst_frame.bits.inst_en      = 1,
		.inst_frame.bits.data_en      = 1,
		.inst_frame.bits.addr_en      = 0,
		.inst_frame.bits.dummy_cycles = 16,
		.inst_frame.bits.tfr_type     = QSPI_READ_ACCESS,
		.inst_frame.bits.width				= QSPI_INST1_ADDR1_DATA1,
		.instruction                  = 0x9f,
		.address                      = 0,
		.buf_len                      = 3,
		.rx_buf                       = qspi_buffer,
	};
	
	transaction_done = false;
	transaction_error = false;
	
	qspi_dma_register_callback(&QUAD_SPI_0, QSPI_DMA_CB_XFER_DONE, xfer_complete_cb_QUAD_SPI_0);
	qspi_dma_register_callback(&QUAD_SPI_0, QSPI_DMA_CB_ERROR, error_cb_QUAD_SPI_0);
	qspi_dma_enable(&QUAD_SPI_0);
	qspi_dma_serial_run_command(&QUAD_SPI_0, &cmd);
	do {
		// Nothing
		} while (!transaction_done && !transaction_error);
		
	if (transaction_error)
	{
		Gateway_Printf("QSPI error",0);
	}
	
	return;
}

/**
 * Example of using GATEWAY_RS485 to write "Hello World" using the IO abstraction.
 */
void GATEWAY_RS485_example(void)
{
	struct io_descriptor *io;
	usart_sync_get_io_descriptor(&GATEWAY_RS485, &io);
	usart_sync_enable(&GATEWAY_RS485);

	io_write(io, (uint8_t *)"Hello World!", 12);
}

/**
 * Example of using ACTUATOR_RS485 to write "Hello World" using the IO abstraction.
 */
void ACTUATOR_RS485_example(void)
{
	struct io_descriptor *io;
	usart_sync_get_io_descriptor(&ACTUATOR_RS485, &io);
	usart_sync_enable(&ACTUATOR_RS485);

	io_write(io, (uint8_t *)"Hello World!", 12);
}

void I2C_0_example(void)
{
	struct io_descriptor *I2C_0_io;

	i2c_m_sync_get_io_descriptor(&I2C_0, &I2C_0_io);
	i2c_m_sync_enable(&I2C_0);
	i2c_m_sync_set_slaveaddr(&I2C_0, 0x12, I2C_M_SEVEN);
	io_write(I2C_0_io, (uint8_t *)"Hello World!", 12);
}
