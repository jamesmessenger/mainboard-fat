#include <atmel_start.h>
#include <hal_io.h>
#include <string.h>
#include "gateway_comms.h"
#include "helpers.h"
#include "addresses.h"
#include "dps310.h"

void run_test(int8_t test);
void select_test();

int main(void)

{
	unsigned char input[10];
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	delay_ms(100);
	
	if(GetMCUNumber() == 1){
		Gateway_Printf("\r\n\033[H\033[2JUtonomy Main Board Test\r\n",0);
		Gateway_Printf("Main MCU present\r\n",0);
		gpio_set_pin_level(INTER_MCU_TX,true); //assert control
		usart_sync_disable(&GATEWAY_RS485);
		delay_ms(150);
	}
	else
	{
		delay_ms(100);
		Gateway_Printf("Backup MCU present\r\n",0);
		usart_sync_disable(&GATEWAY_RS485);
		delay_ms(100);
	}
	
	while (1) {
		
		if(GetMCUNumber() == 1)
		{
			Gateway_Printf("Enter MCU number (1/2):",0);
			Gateway_GetCharacter(input);
			if(input[0] == '1') // message is for MCU 1
			{
				select_test();
			}
			else if (input[0] == '2')
			{
				//Disable RS485
				usart_sync_disable(&GATEWAY_RS485);
				//Throw control to the other MCU
				gpio_set_pin_level(INTER_MCU_TX,false);
				//wait for it to assert control
				delay_ms(100);
				//then wait until it releases control
				while(gpio_get_pin_level(INTER_MCU_RX))
				{

				}
				//then take it back
				gpio_set_pin_level(INTER_MCU_TX,true);
			}
	
			
		}
		
		if(GetMCUNumber() == 2)
		{
			//MCU 2 waits until it's handed control by MCU 1.
			while(gpio_get_pin_level(INTER_MCU_RX)) 
			{

			}
			//Now MCU 2 has control
			gpio_set_pin_level(INTER_MCU_TX,true);
			//Request and run a test
			select_test();
			//then close RS485
			usart_sync_disable(&GATEWAY_RS485);
			///and give up control to MCU 1
			gpio_set_pin_level(INTER_MCU_TX,false);
			delay_ms(100);
		}
				
	}


	
}
void select_test()
{
	int8_t input[10];
	Gateway_Printf("\r\nMCU %i: Enter a command number: ",GetMCUNumber());
	Gateway_Read(input);
	if(check_numeric(input))
	{
		run_test(atoi(input));
	}
	else
	{
		Gateway_Printf("MCU %i Command invalid\r\n",GetMCUNumber());
	}
}

void run_test(int8_t test)
{
	Gateway_Printf("\r\nRunning test %i ", test);
	Gateway_Printf("on MCU %i\r\n", GetMCUNumber());
	int8_t data[5]; //Generic array for data which is sent and received during the tests
	int8_t receivedBytes[3];
	union pressureSensorData {
		int8_t c[4];
		int16_t s[2];
		int32_t l;
	} ;
	

	union pressureSensorData rawPressure;
	union pressureSensorData rawTemperature;
	int32_t truePressure;
	int32_t trueTemperature;
	int8_t returnCount = 0;
	
	switch(test)
	{
		case 1: //Set the RS485 Alert line HIGH
			gpio_set_pin_level(GATEWAY_RS485_ALERT, true);
			Gateway_Printf("Gateway alert pin set HIGH\r\n", 0);
			break;
		case 2: //Report PASS if MAIN_PWR_OK is HIGH and BACKUP_PWR_OK is HIGH
			if(gpio_get_pin_level(MAIN_POWER_OK) && gpio_get_pin_level(BACKUP_POWER_OK))
			{
				Gateway_Printf("PASS: MAIN POWER OK and BACKUP POWER OK signals both HIGH\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: MAIN POWER OK and BACKUP POWER OK signals not both HIGH\r\n", 0);
			}
			break;
			case 3: //Report PASS if MAIN_PWR_OK is LOW and BACKUP_PWR_OK is HIGH
			if(!(gpio_get_pin_level(MAIN_POWER_OK)) && gpio_get_pin_level(BACKUP_POWER_OK))
			{
				Gateway_Printf("PASS: MAIN POWER OK is LOW,  BACKUP POWER OK signal is HIGH\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: MAIN POWER OK is not LOW or BACKUP POWER OK signal is not HIGH\r\n", 0);
			}
			break;
			case 4: //Report PASS if MAIN_PWR_OK is HIGH and BACKUP_PWR_OK is LOW
			if(gpio_get_pin_level(MAIN_POWER_OK) && !(gpio_get_pin_level(BACKUP_POWER_OK)))
			{
				Gateway_Printf("PASS: MAIN POWER OK is HIGH,  BACKUP POWER OK signal is LOW\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: MAIN POWER OK is not HIGH or BACKUP POWER OK signal is not LOW\r\n", 0);
			}
			break;
	
			
		case 5: //Set the RTC, then wait and read the time. PASS if the clock is running.
			data[0] = 0x00;
			data[1] = 0x00;
			i2c_sendDataThroughArbiter(CLOCK_ADDRESS, data, 2);
			data[0] = 0x00;
			data[1] = 0x80;
			i2c_sendDataThroughArbiter(CLOCK_ADDRESS, data, 2);
			i2c_readDataThroughArbiter(CLOCK_ADDRESS, 0x00, &data[0], 1); 
			delay_ms(2000); //Let the clock tick
			i2c_readDataThroughArbiter(CLOCK_ADDRESS, 0x00, &data[1], 1); 
			if(data[1] > data[0]) //Has time passed?
			{
				Gateway_Printf("PASS: RTC successfully set and is running\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: RTC not successfully seen running\r\n", 0);
			}
			break;

		case 6: //Configure power expander pin directions and set all its output pins as LOW.
			data[0] = 0x03;
			data[1] = 0x03;
			returnCount += i2c_sendDataThroughArbiter(EXPANDER_ADDRESS, data, 2);
			data[0] = 0x01;
			data[1] = 0x00;
			returnCount += i2c_sendDataThroughArbiter(EXPANDER_ADDRESS, data, 2);
			
			if (returnCount == 0)
			{
				Gateway_Printf("Port expander configured; BATT_SELx and EXT_BUS1_EN set LOW.\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: Port expander could not be correctly configured", 0);
			}
		
			break;
			
		case 7: //Set BATT_SEL0 high, all other expander outputs low
		//todo: check return value of i2c commands

			data[0] = 0x01;
			data[1] = 0x04;
			if (!(i2c_sendDataThroughArbiter(EXPANDER_ADDRESS, data, 2)))
			{
				Gateway_Printf("BATT_SEL0 set HIGH.\r\n", 0);	
			}
			else
			{
				Gateway_Printf("FAIL: BATT_SEL0 could not be set HIGH.\r\n", 0);
			}
			
			
			break;
			
		case 8: //Set BATT_SEL1 high, all other expander outputs low
		//todo: check return value of i2c commands

			data[0] = 0x01;
			data[1] = 0x08;
			if (!(i2c_sendDataThroughArbiter(EXPANDER_ADDRESS, data, 2)))
			{
				Gateway_Printf("BATT_SEL1 set HIGH.\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: BATT_SEL1 could not be set HIGH.\r\n", 0);
			}
					
			break;
			
		case 9: //Set EXT_BUS_EN high, all other expander outputs low
		//todo: check return value of i2c commands

			data[0] = 0x01;
			data[1] = 0x20;
			if (!(i2c_sendDataThroughArbiter(EXPANDER_ADDRESS, data, 2)))
			{
				Gateway_Printf("EXT_BUS_EN set HIGH.\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: EXT_BUS_EN could not be set HIGH.\r\n", 0);
			}
					
			break;
			
		case 10: //Perform an I2C read from a known slave address, report PASS if the data matches an expected value.
		//todo: check return value of i2c commands
		
			data[0] = 0;
			i2c_readDataThroughArbiter(EXTERNAL_SLAVE_ADDRESS, 0, data, 1);
			
			if (data[0] == EXPECTED_SLAVE_DATA)
			{
				Gateway_Printf("PASS: Expected data was received\r\n",0);
			}
			else
			{
				Gateway_Printf("FAIL: Data did not match expected\r\n",0);	
			}
			break;
			
		case 11: //Configure the ambient pressure sensor and read the pressure and temperature; report PASS if the readings fall within reasonable limits.

			//Do not use arbiter!
		
			//No need to delay for coefficients to be ready, because of existing program startup delays.

			dps310_get_coefficients();
			
			//Configure the device and start the readings.
			data[0] = 0x06;
			data[1] = 0x02;
			data[2] = 0x80;
			data[3] = 0x07;
			data[4] = 0x00;
			i2c_sendData(DPS310_ADDRESS, data, 5);
			
			//Delay to allow the readings to be ready after configuration
			delay_ms(1000);
			
			//check that they are ready.
			i2c_readData(DPS310_ADDRESS, 0x08, data, 1);
			if((data[0] & 0x30) != 0x30)
			{
				Gateway_Printf("FAIL: DPS310 not ready after expected time\r\n", 0);
			}
			
			//Read raw pressure data

			i2c_readData(DPS310_ADDRESS, 0x00, data, 3);
			
			//Flip it around as data is transmitted LSB first
			
			rawPressure.c[2] = data[0];
			rawPressure.c[1] = data[1];
			rawPressure.c[0] = data[2];
			if (rawPressure.l & DPS310_READ_SIGN_MASK)	{ rawPressure.l |= DPS310_READ_SIGN_EXT; } //Perform sign extension
			
			//Read raw temperature data

			i2c_readData(DPS310_ADDRESS, 0x03, data, 3);
						
			//Flip it around as data is transmitted LSB first

			rawTemperature.c[2] = data[0];
			rawTemperature.c[1] = data[1];
			rawTemperature.c[0] = data[2];
			if (rawTemperature.l & DPS310_READ_SIGN_MASK)	{ rawTemperature.l |= DPS310_READ_SIGN_EXT; } //Perform sign extension
		
			//Obtain the true temperature and pressure.
			
			truePressure = dps310_calc_pressure(rawPressure.l, rawTemperature.l);
			trueTemperature = dps310_calc_temperature(rawTemperature.l);
			
			//todo: is printing the actual values necessary?
			Gateway_Printf("Pressure reading %i mBar\r\n", truePressure/100);
			Gateway_Printf("Temperature reading %i C\r\n", (trueTemperature/1000)-273); //this rounds to the nearest degree but it's OK as a rough measurement
			
			
			if((truePressure <= 95000) || (truePressure >= 105000))
			{
				Gateway_Printf("FAIL: Pressure reading outside of limits\r\n", 0);
			}
			else if ((trueTemperature < 273000) || (trueTemperature >= 313000))
			{
				Gateway_Printf("FAIL: Temperature reading outside of limits\r\n", 0);
			}
			else
			{
				Gateway_Printf("PASS: Pressure and temperature readings OK\r\n", 0);
			}
			
			break;
		
		
		case 12: //Set ENCODER_EN HIGH.
			gpio_set_pin_level(ENCODER_EN, true);
			Gateway_Printf("Encoder enable pin set HIGH\r\n", 0);
			break;
		
		case 13: //Send known data over the actuator RS485 interface. Wait for response and match response to expected data.
			receivedBytes[0] = 0;
			receivedBytes[1] = 0;
			receivedBytes[2] = 0;
			Actuator_RS485_TestTx();
			Gateway_Printf("Message sent over actuator %i RS485 interface, awaiting response...\r\n", GetMCUNumber());
			
			if(!(Actuator_RS485_TestRx(receivedBytes, 3)))
			{
				Gateway_Printf("FAIL: Timed out\r\n", 0);
			}
			
			else if(!strcmp(receivedBytes,"123"))
			{
				Gateway_Printf("PASS: Received data as expected\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: Received data not as expected\r\n", 0);
			}
			break;
			
		case 14: //Read ENCODER_ALERT, report PASS if LOW
			if(!gpio_get_pin_level(ENCODER_ALERT))
			{
				Gateway_Printf("PASS: Actuator alert line is LOW\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: Actuator alert line is not LOW as expected\r\n", 0);
			}
			break;
			
		case 15: //Read ENCODER_ALERT, report PASS if HIGH
			if(gpio_get_pin_level(ENCODER_ALERT))
			{
				Gateway_Printf("PASS: Actuator alert line is HIGH\r\n", 0);
			}
			else
			{
				Gateway_Printf("FAIL: Actuator alert line is not HIGH as expected\r\n", 0);
			}
			break;
			
		case 16: //Set MAIN_ISO_EN HIGH, MOTOR1_POS LOW and MOTOR1_NEG LOW.
			Gateway_Printf("Setting MAIN_ISO_EN HIGH, MOTOR1_POS LOW and MOTOR1_NEG LOW.\r\n", 0);
			gpio_set_pin_level(MAIN_ISO_EN, true);
			gpio_set_pin_level(MOTOR1_POS, false);
			gpio_set_pin_level(MOTOR1_NEG, false);
			break;
			
		case 17: //Set MAIN_ISO_EN HIGH, MOTOR1_POS HIGH and MOTOR1_NEG LOW.
			Gateway_Printf("Setting MAIN_ISO_EN HIGH, MOTOR1_POS HIGH and MOTOR1_NEG LOW.\r\n", 0);
			gpio_set_pin_level(MAIN_ISO_EN, true);
			gpio_set_pin_level(MOTOR1_POS, true);
			gpio_set_pin_level(MOTOR1_NEG, false);
			break;
			
		case 18: //Set MAIN_ISO_EN HIGH, MOTOR1_POS LOW and MOTOR1_NEG HIGH.
			Gateway_Printf("Setting MAIN_ISO_EN HIGH, MOTOR1_POS LOW and MOTOR1_NEG HIGH.\r\n", 0);
			gpio_set_pin_level(MAIN_ISO_EN, true);
			gpio_set_pin_level(MOTOR1_POS, false);
			gpio_set_pin_level(MOTOR1_NEG, true);
			break;
			
		case 19: //Set BACKUP_ISO_EN HIGH, MOTOR2_POS LOW and MOTOR2_NEG LOW.
			Gateway_Printf("Setting BACKUP_ISO_EN HIGH, MOTOR2_POS LOW and MOTOR2_NEG LOW.\r\n", 0);
			gpio_set_pin_level(BACKUP_ISO_EN, true);
			gpio_set_pin_level(MOTOR2_POS, false);
			gpio_set_pin_level(MOTOR2_NEG, false);
		break;
				
		case 20: //Set BACKUP_ISO_EN HIGH, MOTOR2_POS HIGH and MOTOR2_NEG LOW.
			Gateway_Printf("Setting BACKUP_ISO_EN HIGH, MOTOR2_POS HIGH and MOTOR2_NEG LOW.\r\n", 0);
			gpio_set_pin_level(BACKUP_ISO_EN, true);
			gpio_set_pin_level(MOTOR2_POS, true);
			gpio_set_pin_level(MOTOR2_NEG, false);
		break;
				
		case 21: //Set BACKUP_ISO_EN HIGH, MOTOR2_POS LOW and MOTOR2_NEG HIGH.
			Gateway_Printf("Setting BACKUP_ISO_EN HIGH, MOTOR2_POS LOW and MOTOR2_NEG HIGH.\r\n", 0);
			gpio_set_pin_level(BACKUP_ISO_EN, true);
			gpio_set_pin_level(MOTOR2_POS, false);
			gpio_set_pin_level(MOTOR2_NEG, true);
		break;
		
		case 22: //Read device ID from QSPI Flash and check it's valid.
			data[0] = 0;
			data[1] = 0;
			data[2] = 0;
			QSPI_Flash_Read_ID(data);
			if (!(data[0] == QSPI_ID_0) && (data[1] == QSPI_ID_1) && (data[2] == QSPI_ID_2))
			{
				Gateway_Printf("FAIL: QSPI Flash did not return correct ID\r\n",0);
			}
			else
			{
				Gateway_Printf("PASS: QSPI Flash returned correct ID\r\n",0);
			}
			break;		
			
		default:
			Gateway_Printf("Not a valid test number\r\n",0);
			break;
		
		
	}
}
