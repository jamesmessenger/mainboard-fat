/*
 * helpers.h
 *
 * Created: 01/10/2019 09:08:58
 *  Author: James Messenger
 */ 


#ifndef HELPERS_H_
#define HELPERS_H_

#include <stdint.h>


int8_t GetMCUNumber(void);
int8_t GetRequestedMCU(void);
void i2c_sendData(int8_t address, int8_t* data, int8_t length);
void i2c_readData(int8_t chipaddress, int8_t readaddress, int8_t* data, int8_t length);
int8_t i2c_sendDataThroughArbiter(int8_t address, int8_t* data, int8_t length);
int8_t i2c_readDataThroughArbiter(int8_t address, int8_t readaddress, int8_t* data, int8_t length);
void QSPI_Flash_Read_ID(int8_t* qspi_buffer);


#endif /* HELPERS_H_ */