/*
 * addresses.h
 *
 * Created: 03/10/2019 15:51:04
 *  Author: James Messenger
 */ 


#ifndef ADDRESSES_H_
#define ADDRESSES_H_

#define ARBITER_ADDRESS 0x73
#define CLOCK_ADDRESS 0x6f
#define EXPANDER_ADDRESS 0x20
#define EXTERNAL_SLAVE_ADDRESS 0x4F
#define DPS310_ADDRESS 0x77

#define EXPECTED_SLAVE_DATA 0x55

#define QSPI_ID_0 0x1F
#define QSPI_ID_1 0x87
#define QSPI_ID_2 0x01





#endif /* ADDRESSES_H_ */