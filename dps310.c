/*
 * dps310.c
 *
 * Created: 07/10/2019 10:04:39
 *  Author: James Messenger
 */ 


#include "addresses.h"
#include "helpers.h"
#include "dps310.h"


int8_t coeffs_raw[26];
DPS310_Coeffs_t coeffs;
volatile DPS310_Coeffs_t *const coeff = &coeffs;

void dps310_get_coefficients()
{
		i2c_readData(DPS310_ADDRESS, 0x10, coeffs_raw, 26);
				
		dps310_read_temperature_coeffs((DPS310_Coeffs_t *const) coeff, &(coeffs_raw[DPS310_C0_OFFSET]));
		dps310_read_pressure_long_coeffs((DPS310_Coeffs_t *const) coeff, &(coeffs_raw[DPS310_C00_OFFSET]));
		coeff->pressure_c01 = dps310_read_pressure_coeff(&(coeffs_raw[DPS310_C01_OFFSET]));
		coeff->pressure_c11 = dps310_read_pressure_coeff(&(coeffs_raw[DPS310_C11_OFFSET]));
		coeff->pressure_c20 = dps310_read_pressure_coeff(&(coeffs_raw[DPS310_C20_OFFSET]));
		coeff->pressure_c21 = dps310_read_pressure_coeff(&(coeffs_raw[DPS310_C21_OFFSET]));
		coeff->pressure_c30 = dps310_read_pressure_coeff(&(coeffs_raw[DPS310_C30_OFFSET]));
}

/*! 
 * \brief Routine to parse the Temperature Coefficients
 *
 * \param [in] coeff - Coefficients Data Structure to store the parsed values
 * \param [in] buffer - Message buffer start of the coefficients
 */
void dps310_read_temperature_coeffs(DPS310_Coeffs_t *const coeff, uint8_t *const buffer)
{
	uint16_t tmp = ((((uint16_t) buffer[0]) << DPS310_C0_MSB_POS) & DPS310_C0_MSB_MASK);
	tmp |= ((((uint16_t) buffer[1]) >> DPS310_C0_LSB_SHFT) & DPS310_C0_LSB_MASK);
	if (tmp & DPS310_SIGN_MASK)	{ tmp |= DPS310_SIGN_EXT; }
	coeff->temperature_c0 = (int16_t) tmp;
	tmp = (((uint16_t) buffer[1] << DPS310_C1_MSB_POS) & DPS310_C1_MSB_MASK);
	tmp |= ((((uint16_t) buffer[2]) >> DPS310_C1_LSB_SHFT) & DPS310_C1_LSB_MASK);
	if (tmp & DPS310_SIGN_MASK)	{ tmp |= DPS310_SIGN_EXT; }
	coeff->temperature_c1 = (int16_t) tmp;
}

/*! 
 * \brief Routine to parse the 24-bits Pressure Coefficients
 *
 * \param [in] coeff - Coefficients Data Structure to store the parsed values
 * \param [in] buffer - Message buffer start of the coefficients
*/
void dps310_read_pressure_long_coeffs(DPS310_Coeffs_t *const coeff, uint8_t *const buffer)
{
	uint32_t tmp = ((((uint32_t) buffer[0]) << DPS310_C00_MSB_POS) & DPS310_C00_MSB_MASK);
	tmp |= ((((uint32_t) buffer[1]) << DPS310_C00_HSB_POS) & DPS310_C00_HSB_MASK);
	tmp |= ((((uint32_t) buffer[2]) >> DPS310_C00_LSB_SHFT) & DPS310_C00_LSB_MASK);
	if (tmp & DPS310_LONG_SIGN_MASK)	{ tmp |= DPS310_LONG_SIGN_EXT; }
	coeff->pressure_c00 = (int32_t) tmp;
	tmp = ((((uint32_t) buffer[2]) << DPS310_C10_MSB_POS) & DPS310_C10_MSB_MASK);
	tmp |= ((((uint32_t) buffer[3]) << DPS310_C10_HSB_POS) & DPS310_C10_HSB_MASK);
	tmp |= ((((uint32_t) buffer[4]) >> DPS310_C10_LSB_SHFT) & DPS310_C10_LSB_MASK);
	if (tmp & DPS310_LONG_SIGN_MASK)	{ tmp |= DPS310_LONG_SIGN_EXT; }
	coeff->pressure_c10 = (int32_t) tmp;
}

/*! 
 * \brief Routine to parse a 16-bit Pressure Coefficient
 *
 * \param [in] buffer - Message buffer start of the coefficient
 * \return int16_t - Parsed Coefficient
*/
int16_t dps310_read_pressure_coeff(uint8_t *const buffer)
{
	uint16_t coeff = ((((uint16_t) buffer[0]) << DPS310_PCOEFF_MSB_POS) & DPS310_PCOEFF_MSB_MASK); 
	coeff |= ((((uint16_t) buffer[1]) << DPS310_PCOEFF_LSB_POS) & DPS310_PCOEFF_LSB_MASK); 
	return ((int16_t) coeff);
}

/*! 
 * \brief Routine to calculate the pressure in Pa from raw readings
 *
 * \param [in] coeff - Coefficients Data Structure
 * \param [in] raw_p - Raw Pressure Reading
 * \param [in] raw_t - Raw Temperature Reading
 * \return uint32_t - Calculated compensated pressure in Pa
*/
int32_t dps310_calc_pressure(int32_t raw_p, int32_t raw_t)
{
	volatile DPS310_Coeffs_t *const coeff = &coeffs;
	/* Calculate term p_raw_sc*(c10 + p_raw_sc*(c20 + p_raw_sc*c30)) */
	int64_t pressure = ((int64_t) coeff->pressure_c00);
	int64_t tmp = (((int64_t) coeff->pressure_c20)*DPS310_PRESSURE_SCALING);
	tmp += (((int64_t) raw_p)*coeff->pressure_c30);
	tmp /= DPS310_PRESSURE_SCALING;
	tmp = (tmp*raw_p) + (((int64_t) coeff->pressure_c10)*DPS310_PRESSURE_SCALING);
	tmp /= DPS310_PRESSURE_SCALING;
	tmp *= raw_p;
	tmp /= DPS310_PRESSURE_SCALING;
	pressure += tmp;
	/* Calculate term t_raw_sc * c01 */
	tmp = (((int64_t) raw_t)*coeff->pressure_c01)/DPS310_TEMPERATURE_SCALING;
	pressure += tmp;
	/* Calculate term t_raw_sc * p_raw_sc * (c11 + p_raw_sc*c21) */
	tmp = ((int64_t) coeff->pressure_c21)*raw_p;
	tmp += (((int64_t) coeff->pressure_c11)*DPS310_PRESSURE_SCALING);
	tmp /= DPS310_PRESSURE_SCALING;
	tmp *= raw_p;
	tmp /= DPS310_PRESSURE_SCALING;
	tmp *= raw_t;
	tmp /= DPS310_TEMPERATURE_SCALING;
	pressure += tmp;
	
	return ((int32_t) pressure);
}

/*! 
 * \brief Routine to calculate the temperature in K from raw readings
 * 
 * To avoid losing precision, the temperature calculation in mK can be calculated as:
 * T (mK) = 500 * (kT * c0 + 2 * c1 * Traw) / kT + 273150. c0 and c1 are 16-bit long and Traw and
 * kT are 24-bit long (maximum) so the total numerator length cannot exceed 51-bit long.
 *
 * \param [in] coeff - Coefficients Data Structure
 * \param [in] raw_t - Raw Temperature Reading
 * \return uint32_t - Calculated compensated temperature in K
*/
int32_t dps310_calc_temperature(int32_t raw_t)
{
	volatile DPS310_Coeffs_t *const coeff = &coeffs;
	int64_t numerator  = ((int64_t) raw_t) * coeff->temperature_c1 * DPS310_TMP_RAW_MULT;
	numerator += coeff->temperature_c0 * DPS310_TEMPERATURE_SCALING;
	numerator *= DPS310_TMP_NUM_MULT;
	
	return ((int32_t) (numerator / DPS310_TEMPERATURE_SCALING) + DPS310_CELSIUS_TO_MILLIKELVIN);
}
