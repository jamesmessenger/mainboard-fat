/*
 * helpers.c
 *
 * Created: 01/10/2019 09:08:45
 *  Author: James Messenger
 */ 

#include <atmel_start.h>
#include <string.h>
#include <hal_io.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "helpers.h"
#include "gateway_comms.h"
#include "addresses.h"

static volatile bool transaction_done;
static volatile bool transaction_error;

static void xfer_complete_cb_QUAD_SPI_0(struct _dma_resource *resource)
{
	transaction_done = true;
}
static void error_cb_QUAD_SPI_0(struct _dma_resource *resource)
{
	transaction_error = true;
}


void QSPI_Flash_Read_ID(int8_t* qspi_buffer)
{
	
	const struct _qspi_command cmd = {
		.inst_frame.bits.inst_en      = 1,
		.inst_frame.bits.data_en      = 1,
		.inst_frame.bits.addr_en      = 0,
		.inst_frame.bits.dummy_cycles = 16,
		.inst_frame.bits.tfr_type     = QSPI_READ_ACCESS,
		.inst_frame.bits.width				= QSPI_INST1_ADDR1_DATA1,
		.instruction                  = 0x9f,
		.address                      = 0,
		.buf_len                      = 3,
		.rx_buf                       = qspi_buffer,
	};
	
	transaction_done = false;
	transaction_error = false;
	
	qspi_dma_register_callback(&QUAD_SPI_0, QSPI_DMA_CB_XFER_DONE, xfer_complete_cb_QUAD_SPI_0);
	qspi_dma_register_callback(&QUAD_SPI_0, QSPI_DMA_CB_ERROR, error_cb_QUAD_SPI_0);
	qspi_dma_enable(&QUAD_SPI_0);
	qspi_dma_serial_run_command(&QUAD_SPI_0, &cmd);
	do {
		// Nothing
		} while (!transaction_done && !transaction_error);
		
	if (transaction_error)
	{
		Gateway_Printf("FAIL: QSPI transaction error",0);
	}
	
	return;
}

//Returns which MCU this program is running on (1 or 2, to match with schematic) 
int8_t GetMCUNumber()
{
	return(gpio_get_pin_level(MCU_ADDRESS)+1);
}

//Retrieves an MCU number and a command number from the user. Checks that the command number is numeric and in range. Returns the requested command number if it's for this MCU, or 0 if it's not / invalid.
int8_t GetRequestedMCU()
{
	struct io_descriptor *io;
	int8_t MessageForThisMCU = false;
	int8_t input[10];
	usart_sync_get_io_descriptor(&GATEWAY_RS485, &io);
	usart_sync_enable(&GATEWAY_RS485);
	
	//Get an MCU number.
	
	Gateway_Read(input);
	
	if(input[0] == '1')
		return 1;
	else if (input[0] == '2')
		return 2;
	else return 0;
	
}

void i2c_sendData(int8_t address, int8_t* data, int8_t length)
{
		struct io_descriptor *I2C_0_io;
		int8_t bytesWritten = 0;

		i2c_m_sync_get_io_descriptor(&I2C_0, &I2C_0_io);
		i2c_m_sync_enable(&I2C_0);
		i2c_m_sync_set_slaveaddr(&I2C_0, address, I2C_M_SEVEN);
		bytesWritten = io_write(I2C_0_io, data, length);
		if(bytesWritten != length)
		{
			Gateway_Printf("FAIL: Could not communicate with slave device (write)\r\n", 0);
			
		}
}

void i2c_readData(int8_t chipaddress, int8_t readaddress, int8_t* data, int8_t length)
{
	struct io_descriptor *I2C_0_io;
	int8_t bytesRead = 0;

	i2c_m_sync_get_io_descriptor(&I2C_0, &I2C_0_io);
	i2c_m_sync_enable(&I2C_0);
	i2c_m_sync_set_slaveaddr(&I2C_0, chipaddress, I2C_M_SEVEN);
	io_write(I2C_0_io, &readaddress, 1);
	bytesRead = io_read(I2C_0_io, data, length);
	if (bytesRead != length)
	{
		Gateway_Printf("FAIL: Could not communicate with slave device (read)\r\n", 0);
		
	}
}

//Carries out an I2C write through the arbiter.
//0 - success
//1 - arbiter would not give lock
//2 - fail; bytes written does not match request
int8_t i2c_sendDataThroughArbiter(int8_t address, int8_t* data, int8_t length)
{
	struct io_descriptor *I2C_0_io;
	
	int8_t arbiterData[2];
	int8_t received;
	int8_t bytesWritten = 0;
	i2c_m_sync_get_io_descriptor(&I2C_0, &I2C_0_io);
	i2c_m_sync_enable(&I2C_0);
	i2c_m_sync_set_slaveaddr(&I2C_0, ARBITER_ADDRESS, I2C_M_SEVEN);
	//Request lock
	arbiterData[0] = 0x81;
	arbiterData[1] = 0x01;
	io_write(I2C_0_io, arbiterData, 2);
	delay_ms(10);
	//Check lock
	io_write(I2C_0_io, arbiterData, 1);
	io_read(I2C_0_io, &received, 1);
	if(received &= 0x02) //Lock granted
	{
		arbiterData[0] = 0x81;
		arbiterData[1] = 0x07;
		io_write(I2C_0_io, arbiterData, 2); //Connect to bus
		arbiterData[0] = 0x84;
		arbiterData[1] = 0x04;
		io_write(I2C_0_io, arbiterData, 2); //Clear lock granted interrupt
		i2c_m_sync_set_slaveaddr(&I2C_0, address, I2C_M_SEVEN);
		bytesWritten = io_write(I2C_0_io, data, length); 
		i2c_m_sync_set_slaveaddr(&I2C_0, ARBITER_ADDRESS, I2C_M_SEVEN);
		arbiterData[0] = 0x81;
		arbiterData[1] = 0x02;
		io_write(I2C_0_io, arbiterData, 2); //Give up the bus
		if(bytesWritten != length)
		{
			Gateway_Printf("FAIL: Could not communicate with slave device (write)\r\n", 0);
			return(2);
		}
	}
	else
	{
		Gateway_Printf("FAIL: Arbiter would not grant lock\r\n", 0);
		return(1);
	}
	
	return(0);
	
}
//Carry out an I2C read through the arbiter.
//Return codes:
//0 - success
//1 - arbiter would not give lock
//2 - fail; bytes read does not match request
int8_t i2c_readDataThroughArbiter(int8_t address, int8_t readaddress, int8_t* data, int8_t length)
{
	struct io_descriptor *I2C_0_io;
	int8_t bytesRead = 0;
	int8_t arbiterData[2];
	int8_t received;
	i2c_m_sync_get_io_descriptor(&I2C_0, &I2C_0_io);
	i2c_m_sync_enable(&I2C_0);
	i2c_m_sync_set_slaveaddr(&I2C_0, ARBITER_ADDRESS, I2C_M_SEVEN);
	//Request lock
	arbiterData[0] = 0x81;
	arbiterData[1] = 0x01;
	io_write(I2C_0_io, arbiterData, 2);
	delay_ms(10);
	//Check lock
	io_write(I2C_0_io, arbiterData, 1);
	io_read(I2C_0_io, &received, 1);
	if(received &= 0x02) //Lock granted
	{
		arbiterData[0] = 0x81;
		arbiterData[1] = 0x07;
		io_write(I2C_0_io, arbiterData, 2); //Connect to bus
		arbiterData[0] = 0x84;
		arbiterData[1] = 0x04;
		io_write(I2C_0_io, arbiterData, 2); //Clear lock granted interrupt
		i2c_m_sync_set_slaveaddr(&I2C_0, address, I2C_M_SEVEN);
		io_write(I2C_0_io, &readaddress, 1); //Send the data
		bytesRead = io_read(I2C_0_io, data, length); //Read the data
		i2c_m_sync_set_slaveaddr(&I2C_0, ARBITER_ADDRESS, I2C_M_SEVEN);
		arbiterData[0] = 0x81;
		arbiterData[1] = 0x02;
		io_write(I2C_0_io, arbiterData, 2); //Give up the bus
		
		if (bytesRead != length)
		{
			Gateway_Printf("FAIL: Could not communicate with slave device (read)\r\n", 0);
			return(2);
		}
	}
	else
	{
		Gateway_Printf("FAIL: Arbiter would not grant lock\r\n", 0);
		return(1);
	}
	
	return(0);
	
}